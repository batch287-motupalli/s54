import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate, Navigate } from 'react-router-dom';

export default function Register() {

	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	// State to determine whether submit button is enabled or not
	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match. 
		if((email !== "" && password1 !== "" && password2 !== "") && (password2 === password1)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	})

	function registerUser(e) {
		e.preventDefault();

			setEmail("");
			setPassword1("");
			setPassword2("");

		alert("Thank you for registering!");
	};
	
	return (
		(user.email !== null) ?
			<Navigate to="/courses" />
		:
			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>

				{ 
					isActive ? 
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" disabled>
						Submit
					</Button>

				}

			</Form>
	)
}